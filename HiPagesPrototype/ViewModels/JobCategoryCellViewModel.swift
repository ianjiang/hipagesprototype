//
//  JobCategoryCellViewModel.swift
//  HiPagesPrototype
//
//  Created by Yinzhi Jiang on 6/2/20.
//  Copyright © 2020 Yinzhi Jiang. All rights reserved.
//

import Foundation

struct JobCategoryCellViewModel {
    var title: String {
        return job.category.rawValue
    }

    var subtitle: String {
        let date = dateFormatter.string(from: job.postedDate)
        return "Posted: \(date)"
    }

    var statusText: String {
        switch job.status {
        case .inProgress:
            return "In Progress"
        case .closed:
            return "Closed"
        }
    }

    var connectedBusinessTitle: String {
        let numberOfConnectedBusiness = job.connectedBusinesses?.count ?? 0
        if numberOfConnectedBusiness <= 0 {
            return "Connecting you with businesses"
        }

        let numberOfHiredBusiness = job.connectedBusinesses?.filter { $0.isHired }.count ?? 0
        return "You have hired \(numberOfHiredBusiness) businesses"
    }

    var connectedBusinesses: [BusinessViewModel] {
        return job.connectedBusinesses?.compactMap { BusinessViewModel(business: $0) } ?? []
    }

    private let job: Job

    private let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter
    }()

    init(job: Job) {
        self.job = job
    }
}
