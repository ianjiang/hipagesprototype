//
//  BusinessViewModel.swift
//  HiPagesPrototype
//
//  Created by Yinzhi Jiang on 6/2/20.
//  Copyright © 2020 Yinzhi Jiang. All rights reserved.
//

import Combine
import Foundation
import UIKit

struct BusinessViewModel {
    var statusText: String? {
        return business.isHired ? "HIRED" : nil
    }

    private let business: Business

    private var token: AnyCancellable?

    init(business: Business) {
        self.business = business
    }

    func loadImage(with completion: @escaping (UIImage?) -> Void) {
        DispatchQueue.global().async {
            guard let data = try? Data(contentsOf: self.business.thumbnailURL) else { return completion(nil) }
            let image = UIImage(data: data)
            return completion(image)
        }
    }
}
