//
//  JobDataProviding.swift
//  HiPagesPrototype
//
//  Created by Yinzhi Jiang on 5/2/20.
//  Copyright © 2020 Yinzhi Jiang. All rights reserved.
//

import Foundation

protocol JobDataProviding {
    var jobs: LiveData<[Job], Error> { get }
}
