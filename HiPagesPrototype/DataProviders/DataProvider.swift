//
//  DataProvider.swift
//  HiPagesPrototype
//
//  Created by Yinzhi Jiang on 5/2/20.
//  Copyright © 2020 Yinzhi Jiang. All rights reserved.
//

import Combine
import Foundation

struct DataProvider {
    private let serviceProviding: ContentServiceProviding

    init(serviceProviding: ContentServiceProviding) {
        self.serviceProviding = serviceProviding
    }
}

extension DataProvider: JobDataProviding {
    var jobs: LiveData<[Job], Error> {
        let servicePublisher: AnyPublisher<JobResponse, Error> = serviceProviding.request(service: .retrieveJobs)
        let jobsPublisher = servicePublisher.flatMap { response -> CurrentValueSubject<[Job], Error> in
            return CurrentValueSubject<[Job], Error>(response.jobs)
        }.eraseToAnyPublisher()

        return LiveData(publisher: jobsPublisher)
    }
}
