//
//  Job.swift
//  HiPagesPrototype
//
//  Created by Yinzhi Jiang on 5/2/20.
//  Copyright © 2020 Yinzhi Jiang. All rights reserved.
//

import Foundation

struct Job: Decodable {
    let id: Int
    let category: JobCategory
    let status: JobStatus
    let postedDate: Date
    let connectedBusinesses: [Business]?

    private enum CodingKeys: String, CodingKey {
        case id = "jobId"
        case category
        case status
        case postedDate
        case connectedBusinesses
    }
}
