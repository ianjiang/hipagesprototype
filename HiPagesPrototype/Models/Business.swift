//
//  Business.swift
//  HiPagesPrototype
//
//  Created by Yinzhi Jiang on 5/2/20.
//  Copyright © 2020 Yinzhi Jiang. All rights reserved.
//

import Foundation

struct Business: Decodable {
    let id: Int
    let thumbnailURL: URL
    let isHired: Bool

    private enum CodingKeys: String, CodingKey {
        case id = "businessId"
        case thumbnailURL = "thumbnail"
        case isHired
    }
}
