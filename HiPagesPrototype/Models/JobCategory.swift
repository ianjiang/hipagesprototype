//
//  JobCategory.swift
//  HiPagesPrototype
//
//  Created by Yinzhi Jiang on 5/2/20.
//  Copyright © 2020 Yinzhi Jiang. All rights reserved.
//

import Foundation

enum JobCategory: String, Decodable {
    case electrician = "Electricians"
    case carpetCleaning = "Carpet Cleaning"
    case splashback = "Splashbacks"
    case painting = "Painting"
}
