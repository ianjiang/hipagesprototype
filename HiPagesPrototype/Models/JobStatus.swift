//
//  JobStatus.swift
//  HiPagesPrototype
//
//  Created by Yinzhi Jiang on 5/2/20.
//  Copyright © 2020 Yinzhi Jiang. All rights reserved.
//

import Foundation

enum JobStatus: Decodable {
    case inProgress
    case closed

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        let string = try container.decode(String.self)

        if string.caseInsensitiveCompare("in progress") == .orderedSame {
            self = .inProgress
            return
        }

        if string.caseInsensitiveCompare("closed") == .orderedSame {
            self = .closed
            return
        }

        throw DecodingError.valueNotFound(JobStatus.self, DecodingError.Context(codingPath: [], debugDescription: ""))
    }
}
