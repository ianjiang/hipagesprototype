//
//  LiveData.swift
//  HiPagesPrototype
//
//  Created by Yinzhi Jiang on 5/2/20.
//  Copyright © 2020 Yinzhi Jiang. All rights reserved.
//

import Combine
import Foundation

class LiveData<Success, Failure: Error>: ObservableObject {
    @Published var success: Success?
    @Published var failure: Failure?

    private var cancellable: AnyCancellable?
    private let publisher: AnyPublisher<Success, Failure>

    init(publisher: AnyPublisher<Success, Failure>) {
        self.publisher = publisher
    }

    func live() {
        cancellable = publisher.sink(receiveCompletion: { completion in
            switch completion {
            case .finished:
                return
            case .failure(let error):
                self.failure = error
            }
        }, receiveValue: { success in
            self.success = success
        })
    }

    func cancel() {
        cancellable?.cancel()
    }
}
