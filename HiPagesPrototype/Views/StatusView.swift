//
//  StatusView.swift
//  HiPagesPrototype
//
//  Created by Yinzhi Jiang on 6/2/20.
//  Copyright © 2020 Yinzhi Jiang. All rights reserved.
//

import UIKit

class StatusView: UIView {
    var statusText: String? {
        didSet {
            statusLabel.text = statusText
            setNeedsLayout()
        }
    }

    private let statusLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12, weight: .semibold)
        label.textColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    init() {
        super.init(frame: .zero)

        backgroundColor = UIColor.orange
        layer.cornerRadius = 10

        addSubview(statusLabel)

        activateConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func activateConstraints() {
        NSLayoutConstraint.activate([
            statusLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            statusLabel.topAnchor.constraint(equalTo: topAnchor, constant: 4),
            statusLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            statusLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -4)
        ])
    }
}
