//
//  BusinessView.swift
//  HiPagesPrototype
//
//  Created by Yinzhi Jiang on 6/2/20.
//  Copyright © 2020 Yinzhi Jiang. All rights reserved.
//

import UIKit

class BusinessView: UIView {
    override var intrinsicContentSize: CGSize {
        return CGSize(width: 60, height: 60)
    }

    private let imageView = UIImageView()
    private let statusView = StatusView()

    init(viewModel: BusinessViewModel) {
        super.init(frame: .zero)

        addSubview(imageView)
        addSubview(statusView)

        configureContents()

        activateConstraints()

        configure(with: viewModel)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func configureContents() {
        imageView.layer.cornerRadius = imageView.bounds.width / 2
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false

        statusView.translatesAutoresizingMaskIntoConstraints = false
    }

    private func activateConstraints() {
        NSLayoutConstraint.activate([
            imageView.leadingAnchor.constraint(equalTo: leadingAnchor),
            imageView.topAnchor.constraint(equalTo: topAnchor),
            imageView.trailingAnchor.constraint(equalTo: trailingAnchor),
            imageView.bottomAnchor.constraint(equalTo: bottomAnchor),
            statusView.centerXAnchor.constraint(equalTo: centerXAnchor),
            statusView.bottomAnchor.constraint(equalTo: imageView.bottomAnchor)
        ])
    }

    private func configure(with viewModel: BusinessViewModel) {
        statusView.statusText = viewModel.statusText
        statusView.isHidden = viewModel.statusText?.isEmpty ?? true
        setNeedsLayout()
        viewModel.loadImage { image in
            DispatchQueue.main.async { [weak self] in
                self?.imageView.image = image
                self?.setNeedsLayout()
            }
        }
    }
}
