//
//  SeparatorView.swift
//  HiPagesPrototype
//
//  Created by Yinzhi Jiang on 6/2/20.
//  Copyright © 2020 Yinzhi Jiang. All rights reserved.
//

import UIKit

class SeparatorView: UIView {
    var text: String? {
        didSet {
            textLabel.text = text
            setNeedsLayout()
        }
    }

    private let textLabel = UILabel()
    private let leftHairLine = UIView()
    private let rightHairLine = UIView()

    override init(frame: CGRect) {
        super.init(frame: frame)

        addSubview(textLabel)
        addSubview(leftHairLine)
        addSubview(rightHairLine)

        configureContents()

        activateConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func configureContents() {
        textLabel.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        textLabel.textColor = UIColor(red: 97/255, green: 191/255, blue: 199/255, alpha: 1.0)
        textLabel.translatesAutoresizingMaskIntoConstraints = false

        leftHairLine.backgroundColor = UIColor.separator
        leftHairLine.translatesAutoresizingMaskIntoConstraints = false

        rightHairLine.backgroundColor = UIColor.separator
        rightHairLine.translatesAutoresizingMaskIntoConstraints = false
    }

    private func activateConstraints() {
        NSLayoutConstraint.activate([
            textLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            textLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            leftHairLine.centerYAnchor.constraint(equalTo: centerYAnchor),
            leftHairLine.leadingAnchor.constraint(equalTo: leadingAnchor),
            leftHairLine.trailingAnchor.constraint(equalTo: textLabel.leadingAnchor, constant: -16),
            leftHairLine.heightAnchor.constraint(equalToConstant: 1),
            rightHairLine.centerYAnchor.constraint(equalTo: centerYAnchor),
            rightHairLine.leadingAnchor.constraint(equalTo: textLabel.trailingAnchor, constant: 16),
            rightHairLine.trailingAnchor.constraint(equalTo: trailingAnchor),
            rightHairLine.heightAnchor.constraint(equalToConstant: 1)
        ])
    }
}
