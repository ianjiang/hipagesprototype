//
//  JobCategoryCell.swift
//  HiPagesPrototype
//
//  Created by Yinzhi Jiang on 6/2/20.
//  Copyright © 2020 Yinzhi Jiang. All rights reserved.
//

import UIKit

class JobCategoryCell: UITableViewCell {
    static let reuseIdentifier = NSStringFromClass(JobCategoryCell.self)

    var viewModel: JobCategoryCellViewModel? {
        didSet {
            configure(with: viewModel)
            setNeedsLayout()
        }
    }

    private let containerView = UIStackView()
    private let titleLabel = UILabel()
    private let subtitleLabel = UILabel()
    private let separator = SeparatorView()
    private let connectedBusinessLabel = UILabel()
    private let businessContainerStackView = UIStackView()
    private let viewDetailsButton = UIButton()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        containerView.addArrangedSubview(titleLabel)
        containerView.addArrangedSubview(subtitleLabel)
        containerView.addArrangedSubview(separator)
        containerView.addArrangedSubview(connectedBusinessLabel)
        containerView.addArrangedSubview(businessContainerStackView)
        containerView.addArrangedSubview(viewDetailsButton)

        contentView.addSubview(containerView)

        configureContents()

        activateConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func configureContents() {
        selectionStyle = .none

        titleLabel.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        titleLabel.textAlignment = .center

        subtitleLabel.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        subtitleLabel.textColor = UIColor.lightGray
        subtitleLabel.textAlignment = .center

        connectedBusinessLabel.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        connectedBusinessLabel.textColor = UIColor.lightGray
        connectedBusinessLabel.textAlignment = .center

        businessContainerStackView.axis = .horizontal
        businessContainerStackView.alignment = .fill
        businessContainerStackView.distribution = .fillEqually
        businessContainerStackView.spacing = 16

        viewDetailsButton.setTitle("View Details", for: .normal)
        viewDetailsButton.setTitleColor(UIColor.orange, for: .normal)
        viewDetailsButton.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .semibold)

        containerView.axis = .vertical
        containerView.alignment = .center
        containerView.distribution = .fillProportionally
        containerView.spacing = 16
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.setCustomSpacing(6, after: titleLabel)
    }

    private func activateConstraints() {
        NSLayoutConstraint.activate([
            containerView.leadingAnchor.constraint(equalTo: contentView.readableContentGuide.leadingAnchor),
            containerView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16),
            containerView.trailingAnchor.constraint(equalTo: contentView.readableContentGuide.trailingAnchor),
            containerView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -16),
            separator.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            separator.trailingAnchor.constraint(equalTo: containerView.trailingAnchor)
        ])
    }

    private func configure(with viewModel: JobCategoryCellViewModel?) {
        guard let vm = viewModel else { return }

        titleLabel.text = vm.title
        subtitleLabel.text = vm.subtitle
        separator.text = vm.statusText
        connectedBusinessLabel.text = vm.connectedBusinessTitle

        businessContainerStackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
        vm.connectedBusinesses.forEach {
            let view = BusinessView(viewModel: $0)
            businessContainerStackView.addArrangedSubview(view)
        }
    }
}
