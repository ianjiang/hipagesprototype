//
//  ViewController.swift
//  HiPagesPrototype
//
//  Created by Yinzhi Jiang on 5/2/20.
//  Copyright © 2020 Yinzhi Jiang. All rights reserved.
//

import Combine
import UIKit

class JobsViewController: UIViewController {
    private let data: LiveData<[Job], Error>
    private var token: AnyCancellable?

    private var dataSource: [JobCategoryCellViewModel] = []

    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.register(JobCategoryCell.self, forCellReuseIdentifier: JobCategoryCell.reuseIdentifier)
        tableView.tableHeaderView = UIView(frame: .zero)
        tableView.backgroundColor = UIColor.systemGroupedBackground
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()

    init(data: LiveData<[Job], Error>) {
        self.data = data
        super.init(nibName: nil, bundle: nil)
        title = "hipages"
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.systemGroupedBackground

        view.addSubview(tableView)

        activateConstraints()

        token = data.objectWillChange.receive(on: DispatchQueue.main).sink(receiveValue: { [weak self] _ in
            guard let jobs = self?.data.success else { return }

            self?.dataSource = jobs.compactMap { JobCategoryCellViewModel(job: $0) }
            self?.tableView.reloadData()
        })
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        data.live()
    }

    private func activateConstraints() {
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
}

extension JobsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSource.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: JobCategoryCell.reuseIdentifier, for: indexPath) as? JobCategoryCell ?? JobCategoryCell()
        cell.viewModel = dataSource[indexPath.section]
        return cell
    }
}

extension JobsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        return view
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 16
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        return view
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 16
    }
}
