//
//  AppCoordinator.swift
//  HiPagesPrototype
//
//  Created by Yinzhi Jiang on 5/2/20.
//  Copyright © 2020 Yinzhi Jiang. All rights reserved.
//

import Foundation
import UIKit

class AppCoordinator {
    var rootViewController: UIViewController {
        let vc = JobsViewController(data: dataProvider.jobs)
        let navigationController = UINavigationController(rootViewController: vc)
        return navigationController
    }

    private let dataProvider: DataProvider

    init(dataProvider: DataProvider) {
        self.dataProvider  = dataProvider
    }
}
