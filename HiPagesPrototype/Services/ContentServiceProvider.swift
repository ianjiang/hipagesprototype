//
//  ContentServiceProvider.swift
//  HiPagesPrototype
//
//  Created by Yinzhi Jiang on 5/2/20.
//  Copyright © 2020 Yinzhi Jiang. All rights reserved.
//

import Combine
import Foundation

struct ContentServiceProvider: ContentServiceProviding {
    private let dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        return dateFormatter
    }()

    func request<Success: ResponseData>(service: ContentService) -> AnyPublisher<Success, Error> {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        return URLSession.shared.dataTaskPublisher(for: service.url)
            .map { $0.data }
            .decode(type: Success.self, decoder: decoder)
            .eraseToAnyPublisher()
    }
}
