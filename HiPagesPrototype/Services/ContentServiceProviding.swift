//
//  ContentServiceProviding.swift
//  HiPagesPrototype
//
//  Created by Yinzhi Jiang on 5/2/20.
//  Copyright © 2020 Yinzhi Jiang. All rights reserved.
//

import Combine
import Foundation

protocol ResponseData: Decodable {}

protocol ContentServiceProviding {
    func request<Success: ResponseData>(service: ContentService) -> AnyPublisher<Success, Error>
}
