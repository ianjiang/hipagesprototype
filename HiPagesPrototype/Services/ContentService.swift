//
//  ContentService.swift
//  HiPagesPrototype
//
//  Created by Yinzhi Jiang on 5/2/20.
//  Copyright © 2020 Yinzhi Jiang. All rights reserved.
//

import Foundation

enum ContentService {
    case retrieveJobs

    var url: URL {
        switch self {
        case .retrieveJobs:
            return URL(string: "https://s3-ap-southeast-2.amazonaws.com/hipgrp-assets/tech-test/jobs.json")!
        }
    }
}
